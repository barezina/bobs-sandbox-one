angular.module('a')
.controller('demoValidationController', function($scope, Restangular) {

    // Set up some variables to hold data in.

    $scope.selectedAddress = null;
    $scope.addresses = [];
    $scope.searchTerm = '';

    // Point restangular to the API.

    Restangular.setBaseUrl('http://localhost:8080/api');

    var defaultHeaders = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-AccessToken': 'Z2dEL3BaaHhpQ0xQNVNYQVo2SE96TmFENVI4bWlJNUlMZnRDbEdVeHYzd0JXWWRwd2JrZ01pdVhCVFh1Z0NqZkNGVnFIbXowSStBVUdYWkRNTE1LMGJIMm1yc05DcUtwN2I5Si9rRTFPRm42L1lPTnc3a0xNaFhlZG4rTzdUNURGWUVWTFhya2ozRkRqWERqdTJyV0NBPT0='
    };

    Restangular.addResponseInterceptor(function(data) {
        return data.data;
    });

    Restangular.setDefaultHeaders(defaultHeaders);

    // Create a function that gets called when typing an address..

    function askGoogle() {

        var payload = {
            address: $scope.searchTerm
        };

        // The next call, will do a GET to the api...
        // https://pharmacy-uat.loyaltyone.com.au/api/locations/validation?address=20_something_street

        Restangular.all('locations').all('validation').getList(payload).then(function(response) {

            // This will print the response from L1 in the console window.
            console.log(response);

            var addressData = response;
            $scope.addresses = [];

            // Do some massaging to get things into an easy array.

            for(i = 0; i < addressData.length; i++) {
                $scope.addresses.push({
                    id: addressData[i].place_id,
                    name: addressData[i].formatted
                });
            }
        });
    };

    function lookupPlace() {

        console.log('lookupPlace');
        console.log($scope.selectedAddress);

        var payload = {
            place: $scope.selectedAddress
        };

        Restangular.all('locations').all('validation').getList(payload).then(function(response) {

            // This will print the response from L1 in the console window.
            console.log(response);

        });
    }

    $scope.askGoogle = askGoogle;
    $scope.lookupPlace = lookupPlace;

});