angular
    .module('a', ['smart-table', 'restangular'])
    .controller('pipeCtrl', ['Resource', function (service) {

        var ctrl = this;

        this.displayed = [];

        this.callServer = function callServer(tableState) {

            ctrl.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.

            service.getPage(start, number, tableState).then(function (result) {
                ctrl.displayed = result.data;
                tableState.pagination.numberOfPages = result.numberOfPages;//set the number of pages so the pagination can update
                ctrl.isLoading = false;
            });
        };

    }])
    .factory('Resource', ['$q', '$filter', '$timeout', 'Restangular', function ($q, $filter, $timeout, Restangular) {

        // this would be the service to call your server, a standard bridge between your model an $http
        // the database (normally on your server)

        var randomsItems = ['supsup'];

        console.log($filter);

        Restangular.setBaseUrl('http://localhost:8080/api');

        Restangular.addResponseInterceptor(function(data) {
            return data.data;
        });

        var defaultHeaders = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-AccessToken': 'Z2dEL3BaaHhpQ0xQNVNYQVo2SE96TmFENVI4bWlJNUlMZnRDbEdVeHYzd0JXWWRwd2JrZ01pdVhCVFh1Z0NqZkNGVnFIbXowSStBVUdYWkRNTE1LMGJIMm1yc05DcUtwN2I5Si9rRTFPRm42L1lPTnc3a0xNaFhlZG4rTzdUNURGWUVWTFhya2ozRkRqWERqdTJyV0NBPT0'
        };

        Restangular.setDefaultHeaders(defaultHeaders);

        //fake call to the server, normally this service would serialize table state to send it to the server (with query parameters for example) and parse the response
        //in our case, it actually performs the logic which would happened in the server
        function getPage(start, number, params) {

            // console.log('start');
            // console.log(start);
            //
            // console.log('number');
            // console.log(number);
            //
            // console.log('params');
            // console.log(params);

            var deferred = $q.defer();

            var filtered = params.search.predicateObject ? $filter('filter')(randomsItems, params.search.predicateObject) : randomsItems;

            if (params.sort.predicate) {
                filtered = $filter('orderBy')(filtered, params.sort.predicate, params.sort.reverse);
            }

            var searchPredicates = {};

            if(typeof(params.search.predicateObject) !== 'undefined') {

                var searchKeys = Object.keys(params.search.predicateObject);

                for(i = 0; i < searchKeys.length; i++) {
                    var currentKey = searchKeys[i];
                    searchPredicates['search[' + currentKey + ']'] = params.search.predicateObject[currentKey];
                }

                console.log('searchPredicates');
                console.log(searchPredicates);

            }

            Restangular.all('members').getList(searchPredicates).then(function(allMembers) {

                deferred.resolve({
                    data: allMembers,
                    numberOfPages: 1
                });

            });

            var result = filtered.slice(start, start + number);

            return deferred.promise;
        }

        return {
            getPage: getPage
        };

    }]);
